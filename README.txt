Commerce add to cart matrix allows you to configure a new way of displaying the add to cart form.

All the possible variation combinations can be displayed in a table and allow instant add to cart for all of those.

The variation combination can be configured from the field formatter.
